<?php

namespace App\DataFixtures;

use App\Entity\ProductCharacteristic;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductCharacteristicFixture extends Fixture
{
    public const WEIGHT_CHARACTERISTIC = 'Weight';

    public const SIZE_CHARACTERISTIC = 'Size';

    public const COLOR_CHARACTERISTIC = 'Color';

    public function load(ObjectManager $manager)
    {
        $characteristics = [
            self::WEIGHT_CHARACTERISTIC => (new ProductCharacteristic())->setName('Weight')->setSlug('weight'),
            self::SIZE_CHARACTERISTIC => (new ProductCharacteristic())->setName('Size')->setSlug('size'),
            self::COLOR_CHARACTERISTIC => (new ProductCharacteristic())->setName('Color')->setSlug('color'),
        ];

        foreach ($characteristics as $characteristic) {
            $manager->persist($characteristic);
        }

        $manager->flush();

        foreach ($characteristics as $code => $characteristic) {
            $this->addReference($code, $characteristic);
        }
    }
}
