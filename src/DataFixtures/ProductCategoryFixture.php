<?php

namespace App\DataFixtures;

use App\Entity\ProductCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductCategoryFixture extends Fixture
{
    public const FOOD_CATEGORY = 'Food';

    public const DRINKS_CATEGORY = 'Drinks';

    public function load(ObjectManager $manager)
    {
        $categories = [
            self::FOOD_CATEGORY => (new ProductCategory())->setName('Food')->setSlug('food'),
            self::DRINKS_CATEGORY => (new ProductCategory())->setName('Drinks')->setSlug('drinks'),
        ];

        foreach ($categories as $category) {
            $manager->persist($category);
        }

        $manager->persist((new ProductCategory())->setName('Alcohol')->setSlug('alcohol'));

        $manager->flush();

        foreach ($categories as $code => $category) {
            $this->addReference($code, $category);
        }
    }
}
