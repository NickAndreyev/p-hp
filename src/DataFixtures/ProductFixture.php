<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProductFixture extends Fixture implements DependentFixtureInterface
{
    public function getDependencies(): array
    {
        return [
            ProductCharacteristicFixture::class,
            ProductCategoryFixture::class,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $weightCharacteristic = $this->getReference(ProductCharacteristicFixture::WEIGHT_CHARACTERISTIC);
        $sizeCharacteristic = $this->getReference(ProductCharacteristicFixture::SIZE_CHARACTERISTIC);
        $colorCharacteristic = $this->getReference(ProductCharacteristicFixture::COLOR_CHARACTERISTIC);

        $foodCategory = $this->getReference(ProductCategoryFixture::FOOD_CATEGORY);
        $drinksCategory = $this->getReference(ProductCategoryFixture::DRINKS_CATEGORY);

        $products = [
            (new Product())->setName('Borgir')->setPrice(100.5)->setProductCharacteristic(new ArrayCollection([$sizeCharacteristic, $colorCharacteristic]))->setProductCategory($foodCategory)->setSlug('borgir'),
            (new Product())->setName('Pitsah')->setPrice(150)->setProductCharacteristic(new ArrayCollection([$sizeCharacteristic, $weightCharacteristic]))->setProductCategory($foodCategory)->setSlug('pitsah'),
            (new Product())->setName('Cola')->setPrice(50.99)->setProductCharacteristic(new ArrayCollection([$sizeCharacteristic]))->setProductCategory($drinksCategory)->setSlug('cola'),
        ];

        foreach ($products as $product) {
            $manager->persist($product);
        }

        $manager->flush();
    }
}
