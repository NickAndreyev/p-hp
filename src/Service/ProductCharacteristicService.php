<?php

namespace App\Service;

use App\Model\IdResponse;
use App\Model\ProductCharacteristic\ProductCharacteristicListResponse;
use App\Model\ProductCharacteristicUpdateRequest;

interface ProductCharacteristicService
{
    public function getProductCharacteristics(): ProductCharacteristicListResponse;

    public function deleteProductCharacteristic(int $id): void;

    public function createProductCharacteristic(ProductCharacteristicUpdateRequest $characteristicUpdateRequest): IdResponse;

    public function updateProductCharacteristic(int $id, ProductCharacteristicUpdateRequest $characteristicUpdateRequest): void;
}
