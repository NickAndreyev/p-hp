<?php

namespace App\Service;

use App\Model\IdResponse;
use App\Model\ProductCategory\ProductCategoryListResponse;
use App\Model\ProductCategoryUpdateRequest;

interface ProductCategoryService
{
    public function getProductCategories(): ProductCategoryListResponse;

    public function deleteProductCategory(int $id): void;

    public function createProductCategory(ProductCategoryUpdateRequest $categoryUpdateRequest): IdResponse;

    public function updateProductCategory(int $id, ProductCategoryUpdateRequest $categoryUpdateRequest): void;
}
