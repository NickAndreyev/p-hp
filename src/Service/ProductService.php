<?php

namespace App\Service;

use App\Model\CreateProductRequest;
use App\Model\IdResponse;
use App\Model\Product\ProductDetails;
use App\Model\Product\ProductListResponse;

interface ProductService
{
    public function getProducts(?int $categoryId, ?int $characteristicId): ProductListResponse;

    public function getProductById(int $id): ProductDetails;

    public function deleteProduct(int $productId): void;

    public function createProduct(CreateProductRequest $request): IdResponse;

    public function updateProduct(int $id, CreateProductRequest $request): void;
}
