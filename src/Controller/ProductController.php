<?php

namespace App\Controller;

use App\Attribute\RequestBody;
use App\Model\CreateProductRequest;
use App\Service\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    public function __construct(private ProductService $productService)
    {
    }

    #[Route(path: '/api/v1/products', methods: ['GET'])]
    public function products(Request $request): Response
    {
        $characteristic = $request->query->get('characteristic');
        $category = $request->query->get('category');

        return $this->json($this->productService->getProducts($category, $characteristic));
    }

    #[Route(path: '/api/v1/product/{id}', methods: ['GET'])]
    public function productById(int $id): Response
    {
        return $this->json($this->productService->getProductById($id));
    }

    #[Route(path: '/api/v1/product/{id}', methods: ['DELETE'])]
    public function deleteProduct(int $id): Response
    {
        $this->productService->deleteProduct($id);

        return $this->json('success');
    }

    #[Route(path: '/api/v1/product', methods: ['PUT'])]
    public function createProduct(#[RequestBody] CreateProductRequest $request): Response
    {
        return $this->json($this->productService->createProduct($request));
    }

    #[Route(path: '/api/v1/product/{id}', methods: ['PATCH'])]
    public function updateProduct(int $id, #[RequestBody] CreateProductRequest $request): Response
    {
        $this->productService->updateProduct($id, $request);

        return $this->json('success');
    }
}
