<?php

namespace App\Controller;

use App\Attribute\RequestBody;
use App\Model\ProductCategoryUpdateRequest;
use App\Service\ProductCategoryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductCategoryController extends AbstractController
{
    public function __construct(private ProductCategoryService $productCategoryService)
    {
    }

    #[Route('/api/v1/productCategories', methods: ['GET'])]
    public function categories(): Response
    {
        return $this->json($this->productCategoryService->getProductCategories());
    }

    #[Route('/api/v1/productCategories/{id}', methods: ['DELETE'])]
    public function deleteProductCategory(int $id): Response
    {
        $this->productCategoryService->deleteProductCategory($id);

        return $this->json('success');
    }

    #[Route('/api/v1/productCategory', methods: ['PUT'])]
    public function createProductCategory(#[RequestBody] ProductCategoryUpdateRequest $categoryUpdateRequest): Response
    {
        return $this->json($this->productCategoryService->createProductCategory($categoryUpdateRequest));
    }

    #[Route('/api/v1/productCategory/{id}', methods: ['PATCH'])]
    public function updateProductCategory(int $id, #[RequestBody] ProductCategoryUpdateRequest $categoryUpdateRequest): Response
    {
        $this->productCategoryService->updateProductCategory($id, $categoryUpdateRequest);

        return $this->json('success');
    }
}
