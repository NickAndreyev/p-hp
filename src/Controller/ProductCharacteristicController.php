<?php

namespace App\Controller;

use App\Attribute\RequestBody;
use App\Model\ProductCharacteristicUpdateRequest;
use App\Service\ProductCharacteristicService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductCharacteristicController extends AbstractController
{
    public function __construct(private ProductCharacteristicService $productCharacteristicService)
    {
    }

    #[Route('/api/v1/product/productCharacteristics')]
    public function characteristics(): Response
    {
        return $this->json($this->productCharacteristicService->getProductCharacteristics());
    }

    #[Route('/api/v1/productCharacteristics/{id}', methods: ['DELETE'])]
    public function deleteProductCategory(int $id): Response
    {
        $this->productCharacteristicService->deleteProductCharacteristic($id);

        return $this->json('success');
    }

    #[Route('/api/v1/productCharacteristic', methods: ['PUT'])]
    public function createProductCategory(#[RequestBody] ProductCharacteristicUpdateRequest $characteristicUpdateRequest): Response
    {
        return $this->json($this->productCharacteristicService->createProductCharacteristic($characteristicUpdateRequest));
    }

    #[Route('/api/v1/productCharacteristic/{id}', methods: ['PATCH'])]
    public function updateProductCategory(int $id, #[RequestBody] ProductCharacteristicUpdateRequest $characteristicUpdateRequest): Response
    {
        $this->productCharacteristicService->updateProductCharacteristic($id, $characteristicUpdateRequest);

        return $this->json('success');
    }
}
