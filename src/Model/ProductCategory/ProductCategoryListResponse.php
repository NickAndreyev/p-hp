<?php

namespace App\Model\ProductCategory;

class ProductCategoryListResponse
{
    /**
     * @var ProductCategory[]
     */
    private array $items;

    /**
     * @param ProductCategory[] $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * @return ProductCategory[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
