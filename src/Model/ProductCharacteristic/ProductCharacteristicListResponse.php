<?php

namespace App\Model\ProductCharacteristic;

class ProductCharacteristicListResponse
{
    /**
     * @var ProductCharacteristic[]
     */
    private array $items;

    /**
     * @param ProductCharacteristic[] $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * @return ProductCharacteristic[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
