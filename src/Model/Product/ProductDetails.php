<?php

namespace App\Model\Product;

use App\Entity\ProductCategory;
use App\Entity\ProductCharacteristic;
use App\Enums\ProductType;
use Doctrine\Common\Collections\Collection;

class ProductDetails
{
    private int $id;

    private string $name;

    private ?float $price;

    private ?ProductCategory $productCategory;

    /**
     * @var Collection<ProductCharacteristic>|null
     */
    private ?Collection $productCharacteristic;

    private ?ProductType $productType;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getProductCategory(): ?ProductCategory
    {
        return $this->productCategory;
    }

    public function setProductCategory(?ProductCategory $productCategory): self
    {
        $this->productCategory = $productCategory;

        return $this;
    }

    /**
     * @return Collection<ProductCharacteristic>|null
     */
    public function getProductCharacteristic(): ?Collection
    {
        return $this->productCharacteristic;
    }

    /**
     * @param Collection<ProductCharacteristic>|null $productCharacteristic
     *
     * @return $this
     */
    public function setProductCharacteristic(?Collection $productCharacteristic): self
    {
        $this->productCharacteristic = $productCharacteristic;

        return $this;
    }

    public function getProductType(): ?ProductType
    {
        return $this->productType;
    }

    public function setProductType(?ProductType $productType): self
    {
        $this->productType = $productType;

        return $this;
    }
}
