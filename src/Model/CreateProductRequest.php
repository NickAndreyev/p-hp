<?php

namespace App\Model;

use App\Enums\ProductType;
use Symfony\Component\Validator\Constraints\NotBlank;

class CreateProductRequest
{
    #[NotBlank]
    private string $name;

    private ?float $price;

    private ?int $productCategoryId;

    private ?array $productCharacteristicIds;

    private ?ProductType $productType;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        if (isset($this->price)) {
            return $this->price;
        }

        return null;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getProductCategoryId(): ?int
    {
        if (isset($this->productCategoryId)) {
            return $this->productCategoryId;
        }

        return null;
    }

    public function setProductCategoryId(?int $productCategoryId): self
    {
        $this->productCategoryId = $productCategoryId;

        return $this;
    }

    public function getProductCharacteristicIds(): ?array
    {
        if (isset($this->productCharacteristicIds)) {
            return $this->productCharacteristicIds;
        }

        return null;
    }

    public function setProductCharacteristicIds(?array $productCharacteristicIds): self
    {
        $this->productCharacteristicIds = $productCharacteristicIds;

        return $this;
    }

    public function getProductType(): ?ProductType
    {
        if (isset($this->productType)) {
            return $this->productType;
        }

        return null;
    }

    public function setProductType(?ProductType $productType): self
    {
        $this->productType = $productType;

        return $this;
    }
}
