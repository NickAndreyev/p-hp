<?php

namespace App\Enums;

enum ProductType: string
{
    case PRODUCT = 'product';

    case DEVICE = 'device';

    case TOY = 'toy';
}
