<?php

namespace App\Exception;

class RequestDeserializeException extends \RuntimeException
{
    public function __construct(\Throwable $throwable)
    {
        parent::__construct('Error while deserializing request body', 0, $throwable);
    }
}
