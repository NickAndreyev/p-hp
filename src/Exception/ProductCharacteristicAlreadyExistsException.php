<?php

namespace App\Exception;

class ProductCharacteristicAlreadyExistsException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('Product already exists');
    }
}
