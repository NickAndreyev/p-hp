<?php

namespace App\Exception;

class ProductCharacteristicNotFoundException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('Product characteristic not found');
    }
}
