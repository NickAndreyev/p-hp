<?php

namespace App\Exception;

class ProductTypeNotFoundException extends \RuntimeException
{
    public function __construct(string $productType)
    {
        parent::__construct("Product type $productType not found");
    }
}
