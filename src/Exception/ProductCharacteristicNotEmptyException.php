<?php

namespace App\Exception;

class ProductCharacteristicNotEmptyException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('Product characteristic has products and can not be deleted');
    }
}
