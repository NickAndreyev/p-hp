<?php

namespace App\Exception;

class ProductAlreadyExistsException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('Product already exists');
    }
}
