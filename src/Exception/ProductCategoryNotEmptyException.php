<?php

namespace App\Exception;

class ProductCategoryNotEmptyException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('Product category has products and can not be deleted');
    }
}
