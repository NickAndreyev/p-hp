<?php

namespace App\Exception;

class ProductCategoryNotFoundException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('Product category not found');
    }
}
