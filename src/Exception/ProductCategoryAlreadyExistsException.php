<?php

namespace App\Exception;

class ProductCategoryAlreadyExistsException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('Product category already exists');
    }
}
