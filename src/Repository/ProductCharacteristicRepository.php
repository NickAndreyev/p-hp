<?php

namespace App\Repository;

use App\Entity\ProductCharacteristic;
use App\Exception\ProductCharacteristicNotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProductCharacteristic>
 *
 * @method ProductCharacteristic|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductCharacteristic|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductCharacteristic[]    findAll()
 * @method ProductCharacteristic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductCharacteristicRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductCharacteristic::class);
    }

    public function save(ProductCharacteristic $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ProductCharacteristic $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getById(int $id): ProductCharacteristic
    {
        $characteristic = $this->find($id);

        if (null === $characteristic) {
            throw new ProductCharacteristicNotFoundException();
        }

        return $characteristic;
    }

    public function getProductCharacteristicsByIds(?array $ids): ?Collection
    {
        $characteristics = [];

        if (isset($id)) {
            foreach ($ids as $id) {
                $characteristic = $this->find($id);

                if (null !== $characteristic) {
                    $characteristics[] = $characteristic;
                }
            }

            return new ArrayCollection($characteristics);
        }

        return null;
    }

    public function existsBySlug(string $slug): bool
    {
        return null !== $this->findOneBy(['slug' => $slug]);
    }

    public function countProductsInCharacteristic(int $characteristicId): int
    {
        return $this->_em->createQuery('select count(p.id) from App\Entity\Product p WHERE :characteristicId MEMBER OF p.productCharacteristic')
            ->setParameter('characteristicId', $characteristicId)
            ->getSingleScalarResult()
        ;
    }
}
