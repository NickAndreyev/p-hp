<?php

namespace App\Repository;

use App\Entity\ProductCategory;
use App\Exception\ProductCategoryNotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProductCategory>
 *
 * @method ProductCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductCategory[]    findAll()
 * @method ProductCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductCategory::class);
    }

    public function save(ProductCategory $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ProductCategory $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getProductCategory(int $categoryId): ProductCategory
    {
        $category = $this->find($categoryId);
        if (null === $category) {
            throw new ProductCategoryNotFoundException();
        }

        return $category;
    }

    public function getById(?int $id): ?ProductCategory
    {
        if (isset($id)) {
            $category = $this->find($id);
            if (null === $category) {
                throw new ProductCategoryNotFoundException();
            }

            return $category;
        }

        return null;
    }

    public function countProductsInCategory($categoryId): int
    {
        return $this->createQueryBuilder('p')
            ->select('count(product.productCategory)')
            ->from('App\Entity\Product', 'product')
            ->andWhere('product.productCategory = :val')
            ->setParameter('val', $categoryId)
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function existsBySlug(string $slug): bool
    {
        return null !== $this->findOneBy(['slug' => $slug]);
    }
}
