<?php

namespace App\Repository;

use App\Entity\Product;
use App\Exception\ProductNotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Product>
 *
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function save(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getById(int $id): Product
    {
        $product = $this->find($id);
        if (null === $product) {
            throw new ProductNotFoundException();
        }

        return $product;
    }

    /**
     * @return Product[]
     */
    public function findProducts(?int $categoryId, ?int $characteristicId): array
    {
        $products = $this->createQueryBuilder('p')->orderBy('p.name', Criteria::ASC);

        if (null !== $categoryId) {
            $products = $products->where('p.productCategory = :val')->setParameter('val', $categoryId);
        }

        if (null !== $characteristicId) {
            $products = $products->andWhere(':characteristicId member of p.productCharacteristic')->setParameter('characteristicId', $characteristicId);
        }

        return $products->getQuery()->getResult();
    }

    public function existsBySlug(string $slug): bool
    {
        return null !== $this->findOneBy(['slug' => $slug]);
    }
}
