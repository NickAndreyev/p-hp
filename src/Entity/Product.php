<?php

namespace App\Entity;

use App\Enums\ProductType;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 100, nullable: false)]
    private string $name;

    #[ORM\Column(type: 'decimal', precision: 10, scale: 2, nullable: true)]
    private ?float $price = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: true)]
    private ?ProductCategory $productCategory = null;

    /**
     * @var Collection<ProductCharacteristic>
     */
    #[ORM\ManyToMany(targetEntity: ProductCharacteristic::class)]
    private Collection $productCharacteristic;

    #[ORM\Column(nullable: true)]
    private ?ProductType $productType;

    #[ORM\Column(length: 255, nullable: false)]
    private string $slug;

    public function __construct()
    {
        $this->productCharacteristic = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getProductCategory(): ?ProductCategory
    {
        return $this->productCategory;
    }

    public function setProductCategory(?ProductCategory $productCategory): self
    {
        $this->productCategory = $productCategory;

        return $this;
    }

    /**
     * @return $this
     */
    public function addProductCharacteristic(?ProductCharacteristic $productCharacteristic): self
    {
        if (!$this->productCharacteristic->contains($productCharacteristic)) {
            $this->productCharacteristic->add($productCharacteristic);
        }

        return $this;
    }

    public function removeProductCharacteristic(ProductCharacteristic $productCharacteristic): self
    {
        $this->productCharacteristic->removeElement($productCharacteristic);

        return $this;
    }

    /**
     * @return Collection<ProductCharacteristic>|null
     */
    public function getProductCharacteristic(): ?Collection
    {
        return $this->productCharacteristic;
    }

    /**
     * @param Collection<ProductCharacteristic>|null $productCharacteristic
     *
     * @return $this
     */
    public function setProductCharacteristic(?Collection $productCharacteristic): self
    {
        if (null !== $productCharacteristic) {
            $this->productCharacteristic = $productCharacteristic;
        }

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getProductType(): ?ProductType
    {
        if (isset($this->productType)) {
            return $this->productType;
        }

        return null;
    }

    public function setProductType(?ProductType $productType): self
    {
        if (null != $productType) {
            $this->productType = $productType;
        }

        return $this;
    }
}
