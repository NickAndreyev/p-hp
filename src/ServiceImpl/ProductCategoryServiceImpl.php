<?php

namespace App\ServiceImpl;

use App\Entity\ProductCategory;
use App\Exception\ProductCategoryAlreadyExistsException;
use App\Exception\ProductCategoryNotEmptyException;
use App\Model\IdResponse;
use App\Model\ProductCategory\ProductCategory as ProductCategoryModel;
use App\Model\ProductCategory\ProductCategoryListResponse;
use App\Model\ProductCategoryUpdateRequest;
use App\Repository\ProductCategoryRepository;
use App\Service\ProductCategoryService;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\String\Slugger\SluggerInterface;

class ProductCategoryServiceImpl implements ProductCategoryService
{
    public function __construct(
        private ProductCategoryRepository $productCategoryRepository,
        private SluggerInterface $slugger)
    {
    }

    public function getProductCategories(): ProductCategoryListResponse
    {
        $categories = $this->productCategoryRepository->findBy([], ['name' => Criteria::ASC]);

        $items = array_map(
            fn (ProductCategory $productCategory) => new ProductCategoryModel(
                $productCategory->getId(), $productCategory->getName(), $productCategory->getSlug()
            ),
            $categories
        );

        return new ProductCategoryListResponse($items);
    }

    public function deleteProductCategory(int $id): void
    {
        $category = $this->productCategoryRepository->getById($id);

        $productsCount = $this->productCategoryRepository->countProductsInCategory($category->getId());

        if ($productsCount > 0) {
            throw new ProductCategoryNotEmptyException();
        }

        $this->productCategoryRepository->remove($category, true);
    }

    public function createProductCategory(ProductCategoryUpdateRequest $categoryUpdateRequest): IdResponse
    {
        $category = new ProductCategory();

        $this->upsertCategory($category, $categoryUpdateRequest);

        return new IdResponse($category->getId());
    }

    public function updateProductCategory(int $id, ProductCategoryUpdateRequest $categoryUpdateRequest): void
    {
        $this->upsertCategory($this->productCategoryRepository->getById($id), $categoryUpdateRequest, false);
    }

    private function upsertCategory(ProductCategory $productCategory, ProductCategoryUpdateRequest $categoryUpdateRequest, bool $checkIfExists = true): void
    {
        $slug = $this->slugger->slug($categoryUpdateRequest->getName());

        if ($checkIfExists && $this->productCategoryRepository->existsBySlug($slug)) {
            throw new ProductCategoryAlreadyExistsException();
        }

        $productCategory->setName($categoryUpdateRequest->getName())->setSlug($slug);

        $this->productCategoryRepository->save($productCategory, true);
    }
}
