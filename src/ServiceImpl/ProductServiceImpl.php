<?php

namespace App\ServiceImpl;

use App\Entity\Product;
use App\Entity\ProductCharacteristic;
use App\Exception\ProductAlreadyExistsException;
use App\Exception\ProductCategoryNotFoundException;
use App\Exception\ProductCharacteristicNotFoundException;
use App\Model\CreateProductRequest;
use App\Model\IdResponse;
use App\Model\Product\ProductDetails;
use App\Model\Product\ProductListResponse;
use App\Model\ProductCharacteristic\ProductCharacteristic as ProductCharacteristicModel;
use App\Repository\ProductCategoryRepository;
use App\Repository\ProductCharacteristicRepository;
use App\Repository\ProductRepository;
use App\Service\ProductService;
use Symfony\Component\String\Slugger\SluggerInterface;

class ProductServiceImpl implements ProductService
{
    public function __construct(
        private ProductRepository $productRepository,
        private ProductCharacteristicRepository $productCharacteristicRepository,
        private ProductCategoryRepository $productCategoryRepository,
        private SluggerInterface $slugger)
    {
    }

    public function getProducts(?int $categoryId, ?int $characteristicId): ProductListResponse
    {
        if (null !== $categoryId) {
            $category = $this->productCategoryRepository->find($categoryId);

            if (null === $category) {
                throw new ProductCategoryNotFoundException();
            }
        }

        if (null !== $characteristicId) {
            $characteristic = $this->productCharacteristicRepository->find($characteristicId);

            if (null === $characteristic) {
                throw new ProductCharacteristicNotFoundException();
            }
        }

        $items = array_map(
            [$this, 'map'],
            $this->productRepository->findProducts($categoryId, $characteristicId)
        );

        return new ProductListResponse($items);
    }

    public function getProductById(int $id): ProductDetails
    {
        $product = $this->productRepository->getById($id);
        $category = $this->productCategoryRepository->getProductCategory($product->getProductCategory()->getId());

        $characteristics = $product->getProductCharacteristic()->map(
            fn (ProductCharacteristic $productCharacteristic) => new ProductCharacteristicModel(
                $productCharacteristic->getId(), $productCharacteristic->getName(), $productCharacteristic->getSlug()
            )
        );

        return (new ProductDetails())
            ->setId($product->getId())
            ->setName($product->getName())
            ->setPrice($product->getPrice())
            ->setProductCharacteristic($characteristics)
            ->setProductCategory($category);
    }

    public function deleteProduct(int $productId): void
    {
        $product = $this->productRepository->getById($productId);

        $this->productRepository->remove($product, true);
    }

    public function createProduct(CreateProductRequest $request): IdResponse
    {
        $product = new Product();

        $this->upsertProduct($product, $request);

        return new IdResponse($product->getId());
    }

    public function updateProduct(int $id, CreateProductRequest $request): void
    {
        $this->upsertProduct($this->productRepository->getById($id), $request, false);
    }

    private function upsertProduct(Product $product, CreateProductRequest $createProductRequest, bool $checkIfExists = true): void
    {
        $slug = $this->slugger->slug($createProductRequest->getName());

        if ($checkIfExists && $this->productRepository->existsBySlug($slug)) {
            throw new ProductAlreadyExistsException();
        }

        $product->setName($createProductRequest->getName())
            ->setPrice($createProductRequest->getPrice())
            ->setProductCategory($this->productCategoryRepository->getById($createProductRequest->getProductCategoryId()))
            ->setProductCharacteristic($this->productCharacteristicRepository->getProductCharacteristicsByIds($createProductRequest->getProductCharacteristicIds()))
            ->setProductType($createProductRequest->getProductType())
            ->setSlug($slug);

        $this->productRepository->save($product, true);
    }

    private function map(Product $product): ProductDetails
    {
        return (new ProductDetails())
            ->setId($product->getId())
            ->setName($product->getName())
            ->setPrice($product->getPrice())
            ->setProductCharacteristic($product->getProductCharacteristic())
            ->setProductCategory($product->getProductCategory())
        ;
    }
}
