<?php

namespace App\ServiceImpl;

use App\Entity\ProductCharacteristic;
use App\Exception\ProductCharacteristicAlreadyExistsException;
use App\Exception\ProductCharacteristicNotEmptyException;
use App\Model\IdResponse;
use App\Model\ProductCharacteristic\ProductCharacteristic as ProductCharacteristicModel;
use App\Model\ProductCharacteristic\ProductCharacteristicListResponse;
use App\Model\ProductCharacteristicUpdateRequest;
use App\Repository\ProductCharacteristicRepository;
use App\Service\ProductCharacteristicService;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\String\Slugger\SluggerInterface;

class ProductCharacteristicServiceImpl implements ProductCharacteristicService
{
    public function __construct(
        private ProductCharacteristicRepository $productCharacteristicRepository,
        private SluggerInterface $slugger)
    {
    }

    public function getProductCharacteristics(): ProductCharacteristicListResponse
    {
        $characteristics = $this->productCharacteristicRepository->findBy([], ['name' => Criteria::ASC]);
        $items = array_map(
            fn (ProductCharacteristic $productCharacteristic) => new ProductCharacteristicModel(
                $productCharacteristic->getId(), $productCharacteristic->getName(), $productCharacteristic->getSlug()
            ),
            $characteristics
        );

        return new ProductCharacteristicListResponse($items);
    }

    public function deleteProductCharacteristic(int $id): void
    {
        $characteristic = $this->productCharacteristicRepository->getById($id);

        $productsCount = $this->productCharacteristicRepository->countProductsInCharacteristic($characteristic->getId());

        if ($productsCount > 0) {
            throw new ProductCharacteristicNotEmptyException();
        }

        $this->productCharacteristicRepository->remove($characteristic, true);
    }

    public function createProductCharacteristic(ProductCharacteristicUpdateRequest $characteristicUpdateRequest): IdResponse
    {
        $characteristic = new ProductCharacteristic();

        $this->upsertCharacteristic($characteristic, $characteristicUpdateRequest);

        return new IdResponse($characteristic->getId());
    }

    public function updateProductCharacteristic(int $id, ProductCharacteristicUpdateRequest $characteristicUpdateRequest): void
    {
        $this->upsertCharacteristic($this->productCharacteristicRepository->getById($id), $characteristicUpdateRequest, false);
    }

    private function upsertCharacteristic(ProductCharacteristic $productCharacteristic, ProductCharacteristicUpdateRequest $characteristicUpdateRequest, bool $checkIfExists = true): void
    {
        $slug = $this->slugger->slug($characteristicUpdateRequest->getName());

        if ($checkIfExists && $this->productCharacteristicRepository->existsBySlug($slug)) {
            throw new ProductCharacteristicAlreadyExistsException();
        }

        $productCharacteristic->setName($characteristicUpdateRequest->getName())->setSlug($slug);

        $this->productCharacteristicRepository->save($productCharacteristic, true);
    }
}
